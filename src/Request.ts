import http, { ClientRequest } from "node:http"
import https from "node:https"
import { InifinityObject } from "@android4682/typescript-toolkit"
import { IncomingHttpHeaders, IncomingMessage } from "node:http";
import { BetterConsole } from "@android4682/better-console"

export interface RequestResult
{
  body: string | InifinityObject
  headers: IncomingHttpHeaders
}

export class Request {
  public static get(uri: string, headers: InifinityObject = {}): Promise<RequestResult>
  {
    return new Promise((resolve, reject) => {
      this.getRaw(uri, headers).then((args) => {
        const [buffer, res] = args
        let body = buffer.toString()
        switch (res.headers['content-type']) {
          case 'application/json':
            body = JSON.parse(body);
            break;
        }
        if (res.statusCode) {
          if (res.statusCode >= 400) reject({
            error: res.statusCode,
            body
          })
          else if (res.statusCode === 301 || res.statusCode === 302 || res.statusCode === 303 || res.statusCode === 307 || res.statusCode === 308) Request.get(<string> res.headers['location'], headers).then((rr) => resolve(rr)).catch((e) => reject(e))
          else resolve({
            body,
            "headers": res.headers
          })
        } else {
          resolve({
            body,
            "headers": res.headers
          })
        }
      })
    })
  }

  public static getRaw(uri: string, headers: InifinityObject = {}): Promise<[Buffer, IncomingMessage]>
  {
    return new Promise((resolve, reject) => {
      const httpHandler = (uri.startsWith('https')) ? https : http
      let request = httpHandler.request(uri, {
        "method": "GET",
        headers
      }, (res) => {
        BetterConsole.debug(`HTTP request returned with ${res.statusCode}`)
        let chunks: any[] = []
        res.on('data', (chunk: any) => chunks.push(chunk))
        res.on('end', () => {
          BetterConsole.debug(`HTTP request returned with ${res.statusCode}`)
          const bufferResult = Buffer.concat(chunks)
          if (res.statusCode) {
            if (res.statusCode === 301 || res.statusCode === 302 || res.statusCode === 303 || res.statusCode === 307 || res.statusCode === 308) {
              Request.getRaw(<string> res.headers['location'], headers).then((rr) => resolve(rr)).catch((e) => reject(e))
              return
            }
          }
          resolve([bufferResult, res])
        })
      })
      request.on("error", (e) => reject({error: e, res: {statusCode: 500}}))
      request.end()
    })
  }

  public static post(uri: string, headers = {}, body: InifinityObject | string, contentType: string = "text/plain"): Promise<RequestResult>
  {
    return new Promise((resolve, reject) => {
      this.postRaw(uri, headers, body, contentType).then((args) => {
        const [buffer, res] = args
        let body = buffer.toString()
        switch (res.headers['content-type']) {
          case 'application/json':
            body = JSON.parse(body);
            break;
        }
        if (res.statusCode) {
          if (res.statusCode >= 400) reject({
            error: res.statusCode,
            body
          })
          else if (res.statusCode === 301 || res.statusCode === 302 || res.statusCode === 303 || res.statusCode === 307 || res.statusCode === 308) Request.get(<string> res.headers['location'], headers).then((rr) => resolve(rr)).catch((e) => reject(e))
          else resolve({
            body,
            "headers": res.headers
          })
        } else {
          resolve({
            body,
            "headers": res.headers
          })
        }
      })
    })
  }

  public static postRaw(uri: string, headers = {}, body: InifinityObject | string, contentType: string = "text/plain"): Promise<[Buffer, IncomingMessage]>
  {
    return new Promise((resolve, reject) => {
      if (typeof body === "object") {
        body = JSON.stringify(body)
        contentType = "application/json"
      }
      const httpHandler = (uri.startsWith('https')) ? https : http
      let request = httpHandler.request(uri, {
        "method": "POST",
        "headers": {
          "Content-Type": contentType,
          "Content-Length": body.length,
          ...headers
        }
      }, (res) => {
        BetterConsole.debug(`HTTP request returned with ${res.statusCode}`)
        let chunks: any[] = []
        res.on('data', (chunk: any) => chunks.push(chunk))
        res.on('end', () => {
          BetterConsole.debug(`HTTP request returned with ${res.statusCode}`)
          let bufferResult = Buffer.concat(chunks)
          
          if (res.statusCode) {
            if (res.statusCode === 301 || res.statusCode === 302 || res.statusCode === 303 || res.statusCode === 307 || res.statusCode === 308) {
              Request.postRaw(<string> res.headers['location'], headers, body, contentType).then((rr) => resolve(rr)).catch((e) => reject(e))
              return
            }
          }

          resolve([bufferResult, res])
        })
      })
      request.on("error", (e) => reject({error: e, res: {statusCode: 500}}))
      request.write(body)
      request.end()
    })
  }

  public static createArgumentString(args: InifinityObject = {}): string
  {
    let argStr = ""
    let amount = 0
    for (const key in args) {
      if (Object.hasOwnProperty.call(args, key)) {
        const arg = args[key];
        if (!arg) continue
        if (amount === 0) argStr += `?`
        else argStr += `&`
        argStr += `${key}=${arg}`
        amount++
      }
    }
    return argStr
  }
}